# common helper functions 

function info {
    echo "INFO: $(date +'%Y-%m-%d %H:%M:%S %Z') $*"
}

function fatal {
    echo "ERROR: $(date +'%Y-%m-%d %H:%M:%S %Z') $*"
    exit 1
}

function deploy_file_to_af {
    local AF_BASE_URL="$1"
    local AF_FILE_PATH="$2"
    local LOCAL_FILE_PATH="$3"

    [ -f "$LOCAL_FILE_PATH" ] || fatal "File $LOCAL_FILE_PATH is not found"
	
    curl -H "X-JFrog-Art-Api: ${JFROG_ART_API}" -X PUT -T "$LOCAL_FILE_PATH" "$AF_BASE_URL/$AF_FILE_PATH/"
}

MY_DIR=$(cd $(dirname "$0") && pwd)
REPO_DIR=$(dirname $MY_DIR)

export DEVOPS_BUILD_ID=$CI_JOB_ID
DEVOPS_COMMIT_HASH=$CI_COMMIT_SHA
export DEVOPS_COMMIT_HASH_SHORT=${CI_COMMIT_SHA:0:8}
DEVOPS_REPO_URL=$(echo $CI_REPOSITORY_URL | cut -d@ -f2-)
DEVOPS_AF_BASE_URL=https://artifactory.mana.ericssondevops.com/artifactory/vz-mtas/
DEVOPS_AF_DIR=cac
DEVOPS_PKG_SUFFIX=pkg
DEVOPS_REPO_NAME=ETASGUI

DEVOPS_DIR="$REPO_DIR"/.devops
DEVOPS_WS_DIR="$REPO_DIR"/.devops/.workspace
DEVOPS_PKG_WS_DIR="$DEVOPS_WS_DIR"/packaging

[ -d "$DEVOPS_DIR" ] || fatal "Malformed repo, $DEVOPS_DIR directory is missing"
[ -d "$DEVOPS_WS_DIR" ] || mkdir -p "$DEVOPS_WS_DIR"
[ -d "$DEVOPS_PKG_WS_DIR" ] || mkdir -p "$DEVOPS_PKG_WS_DIR"

# Dump the DEVOPS envvars
env | grep ^DEVOPS_ | sort
