#!/bin/sh

. $(dirname $0)/common-lib.sh

info "Starting the codescan phase..."

export SONAR_RUNNER_OPTS="-Xmx8G"
sonar-scanner

if [ "$?" -ne 0 ]
then
  info "Successfully finished the codescan phase..."
  exit 1
fi

info "Successfully finished the codescan phase..."
