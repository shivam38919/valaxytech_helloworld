#!/bin/bash -ex

. $(dirname $0)/common-lib.sh

info "Starting the build phase..."
cd  /server/src/main
mvn \
    -B \
    -f "$REPO_DIR"/pom.xml \
    -Dmaven.test.skip=true \
    clean \
    install
info "Successfully finished the build phase..."